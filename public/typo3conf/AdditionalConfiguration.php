<?php
use \TYPO3\CMS\Core\Utility\GeneralUtility;

$GLOBALS['TYPO3_CONF_VARS']['SYS']['trustedHostsPattern'] = '.*';

$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default'] = array_merge(
        $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default'] ?? [],
        [
                'dbname' => 'db',
                'host' => 'db',
                'password' => 'db',
                'port' => '3306',
                'user' => 'db',
                'initCommands' => 'SET sql_mode=\'\';',
        ]
);

$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport'] = 'smtp';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_server'] = 'localhost:1025';

$GLOBALS['TYPO3_CONF_VARS']['MAIL']['templateRootPaths'][700] = 'EXT:play_dashboard/Resources/Private/Templates/Mail';

$GLOBALS['play_dashboard']['AdminUid'] = 66;

if (\TYPO3\CMS\Core\Core\Environment::getContext() == 'Production/Stage') {
    include '/home/www/p601849/html/config/Stage/AdditionalConfiguration.php';
}

if (\TYPO3\CMS\Core\Core\Environment::getContext() == 'Production/Live') {
    include '/home/www/p601849/html/config/Live/AdditionalConfiguration.php';
}
